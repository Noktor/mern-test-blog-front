import React, { useEffect } from 'react';
import Toolbar from '@material-ui/core/Toolbar';
import AppBar from '@material-ui/core/AppBar';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';
import SessionRegisterModal from '../../Session/components/modals/RegisterModal';
import SessionLoginModal from '../../Session/components/modals/LoginModal';
import AvatarModal from '../../Session/components/modals/AvatarModal';
import SessionLogoutFragment from '../../Session/components/fragments/LogoutFragment';
import { useDispatch, useSelector } from 'react-redux';

// Import Actions
import { loadUserRequest, registerUserRequest, loginUserRequest, logoutUser, setNewAvatarRequest } from '../../Session/SessionActions';
import { returnErrors } from '../../util/ErrorActions';

// Import Constants
import { USER_LOADING, AUTH_ERROR } from '../../util/actionTypes';

function Navbar() {

    const dispatch = useDispatch();

    const state = useSelector(state => state);
    const token = useSelector(state => state.auth.token);

    useEffect(() => {
        dispatch({ type: USER_LOADING });
        dispatch(loadUserRequest(token));
    },[]);    

    const handleUserRegister = state => {
        dispatch(registerUserRequest(state));
    };

    const handleUserLogin = state => {
        dispatch(loginUserRequest(state));
    };

    const handleSetNewAvatar = (data) => {
        dispatch(setNewAvatarRequest(data, token));
    };

    const handleUserLogout = () => {
        dispatch(logoutUser());
    };

    return (
        <AppBar position="fixed">
            <Toolbar>
                <Typography variant="h6" >
                    <Link href="/" className="text-white">Home</Link>
                </Typography>
                { !state.auth.user ?
                    <Grid container justify="flex-end">
                        <SessionRegisterModal
                            registerUser={handleUserRegister}
                            />
                        <SessionLoginModal
                            loginUser={handleUserLogin}
                            />
                    </Grid>
                    :
                    ''
                }
                { state.auth.user ?
                    <Grid container justify="flex-end">
                        <AvatarModal setNewAvatar={handleSetNewAvatar}></AvatarModal>
                        <strong>
                            Welcome {state.auth.user.username}
                        </strong>
                        <Typography variant="h6" >
                        <SessionLogoutFragment
                            logoutUser={handleUserLogout}
                        />
                        </Typography>
                    </Grid>
                    :
                    ''
                }
            </Toolbar>
        </AppBar>
    );
};

export default Navbar;