import callApi from '../util/apiCaller';

// Export Constants
export const ADD_POST = 'ADD_POST';
export const LIKE_POST = 'LIKE_POST';
export const ADD_POSTS = 'ADD_POSTS';
export const DELETE_POST = 'DELETE_POST';
export const ADD_IMAGE = 'ADD_IMAGE';
export const DELETE_IMAGE = 'DELETE_IMAGE';
export const LIKE_IMAGE = 'LIKE_IMAGE';

// Export Actions
export function addImage(image) {
  return {
    type: ADD_IMAGE,
    image,
  };
}

export function addImageRequest(data, token) {
  return (dispatch) => {
    return callApi('posts/image', 'post', {
      data
    }, token).then(res => dispatch(addImage(res.image)));
  };
}

export function deleteImage(publicId) {
  return {
    type: DELETE_IMAGE,
    publicId
  };
}

export function deleteImageRequest(data, token) {
  return (dispatch) => {
    return callApi(`posts/image/${data.cuid}/${data.publicId}`, 'delete', null, token).then(() => dispatch(deleteImage(data.publicId)));
  };
}

export function likeImage(publicId) {
  return {
    type: LIKE_IMAGE,
    publicId
  };
}

export function likeImageRequest(data, token) {
  return (dispatch) => {
    return callApi(`posts/image/like/${data.cuid}/${data.publicId}`, 'get', null, token).then(() => dispatch(likeImage(data.publicId)));
  };
}

export function addPost(post) {
  return {
    type: ADD_POST,
    post,
  };
}

export function addPostRequest(post, token) {
  return (dispatch) => {
    return callApi('posts', 'post', {
      post: {
        name: post.name,
        title: post.title,
        content: post.content,
      }
    }, token).then(res => dispatch(addPost(res.post)));
  };
}

export function likePost(publicId) {
  return {
    type: LIKE_POST,
    publicId
  };
}

export function likePostRequest(cuid, token) {
  return (dispatch) => {
    return callApi(`posts/like/${cuid}`, 'get', null, token).then(() => dispatch(likePost(cuid)));
  };
}

export function addPosts(posts) {
  return {
    type: ADD_POSTS,
    posts,
  };
}

export function fetchPosts() {
  return (dispatch) => {
    return callApi('posts').then(res => {
      dispatch(addPosts(res.posts));
    });
  };
}

export function fetchPost(cuid) {
  return (dispatch) => {
    return callApi(`posts/${cuid}`).then(res => dispatch(addPost(res.post)));
  };
}

export function deletePost(cuid) {
  return {
    type: DELETE_POST,
    cuid,
  };
}

export function deletePostRequest(cuid, token) {
  return (dispatch) => {
    return callApi(`posts/${cuid}`, 'delete', null, token).then(() => dispatch(deletePost(cuid)));
  };
}
