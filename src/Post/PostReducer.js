import { ADD_IMAGE, ADD_POST, ADD_POSTS, LIKE_POST, DELETE_POST, DELETE_IMAGE, LIKE_IMAGE } from './PostActions';

// Initial State
const initialState = { data: [] };

const PostReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_POST :
      if(!state.data) return { data: state};
      return {
        data: [action.post, ...state.data],
      };

    case ADD_POSTS :
      return {
        data: action.posts,
      };

    case LIKE_POST :
    case DELETE_POST :
    case ADD_IMAGE :
    case DELETE_IMAGE :
    case LIKE_IMAGE :
      return {
        data: state.data.filter(post => post.cuid !== action.cuid)
      }
    default:
      return state;
  }
};

/* Selectors */

// Get all posts
export const getPosts = state => state.posts.data;

// Get post by cuid
export const getPost = (state, cuid) => state.posts.data.filter(post => post.cuid === cuid)[0];

// Export Reducer
export default PostReducer;
