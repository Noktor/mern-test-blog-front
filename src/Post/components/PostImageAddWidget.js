import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
// Import Selectors
import { useParams } from 'react-router-dom';

const useStyles = makeStyles(theme => ({
    root: {
        '& > *': {
            margin: theme.spacing(1),
        },
    },
}));

const PostAddImageWidget = ({ addImage }) => {

  const [state, setState] = useState({});
  const [previewSource, setPreviewSource] = useState('');
  const [selectedFile, setSelectedFile] = useState();

  //posar styles:
  const classes = useStyles();

  const { cuid } = useParams();

  state.cuid = cuid;

  const fileSelectedHandler = (e) => {
    const file = e.target.files[0];
    setSelectedFile(file);
    previewFile(file);
  }

  const previewFile = (file) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onloadend = () => {
          setPreviewSource(reader.result);
      };
  };

  const fileUploadHandler = (e) => {
    e.preventDefault();
    if (!selectedFile) return;
    const reader = new FileReader();
    reader.readAsDataURL(selectedFile);
    reader.onloadend = () => {
        uploadImage(reader.result);
    };
    reader.onerror = () => {
    };
  };

  const uploadImage = async (base64EncodedImage) => {
    setState({
      ...state,
      image: base64EncodedImage
    })
    addImage({image: base64EncodedImage, cuid: cuid});
    setPreviewSource('');
  };

  return (
    <div>
      <form encType="multipart/form-data">
        <input type="file" onChange={fileSelectedHandler}/>
          <button
            onClick={fileUploadHandler}
            type="button">
            Upload Image to gallery
          </button>
      </form>
      {previewSource && (
          <img
              src={previewSource}
              alt="chosen"
              style={{ height: '300px' }}
          />
      )}
    </div>
  );
};

PostAddImageWidget.propTypes = {
  addImage: PropTypes.func.isRequired
};

export default PostAddImageWidget;