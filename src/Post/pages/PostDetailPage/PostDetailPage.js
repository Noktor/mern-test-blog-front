import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import ListSubheader from '@material-ui/core/ListSubheader';
import DeleteForeverOutlinedIcon from '@material-ui/icons/DeleteForeverOutlined';
import ThumbUpAltOutlinedIcon from '@material-ui/icons/ThumbUpAltOutlined';

// Import Components
import PostAddImageWidget from '../../components/PostImageAddWidget';

// Import Actions
import { fetchPosts } from '../../PostActions';

// Import Selectors
import { useParams } from 'react-router-dom';

import { addImageRequest, deleteImageRequest, likeImageRequest } from '../../PostActions';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    overflow: 'hidden',
    backgroundColor: theme.palette.background.paper,
  },
  gridList: {
    width: 500,
    height: 450,
  },
  icon: {
    color: theme.palette.text.action
  }
}));

export function PostDetailPage() {

  const classes = useStyles();
  const { cuid } = useParams(); 
  const dispatch = useDispatch();
  const post = useSelector(state => state.posts.data.find(currentPost => (currentPost.cuid === cuid)));
  const token = useSelector(state => state.auth.token);
  const activeUser = useSelector(state => state.auth.user);

  useEffect(() => {
    dispatch(fetchPosts());
  }, []);

  const handleAddImage = async (data) => {
    await dispatch(addImageRequest(data, token));
    dispatch(fetchPosts());
  };

  const handleDeleteImage = async (cuid, publicId) => {
    if (confirm('Do you want to delete this image')) { // eslint-disable-line
      await dispatch(deleteImageRequest({cuid, publicId}, token));
      dispatch(fetchPosts());
    }
  };

  const handleLikeImage = async (cuid, publicId) => {
    if (confirm('Do you want to like this image (pls don\'t spam!)')) { // eslint-disable-line
      await dispatch(likeImageRequest({cuid, publicId}, token));
      dispatch(fetchPosts());
    }
  };

  return (post
    ?
      (<div className="container">
        <div className="row">
          <div className="col-12">
            <h1>{post.title}</h1>
            <p>By {post.name}</p>
            <p>{post.content}</p>
            {
              post.images.length !== 0 ?
                <div className={classes.root}>
                  <GridList cellHeight={180} className={classes.gridList}>
                    <GridListTile key="Subheader" cols={2} style={{ height: 'auto' }}>
                      <ListSubheader component="div">Gallery</ListSubheader>
                    </GridListTile>
                    {post.images.map(img => (
                      <GridListTile key={img.url}>
                        <img src={img.url} />
                        <GridListTileBar
                          title={
                            img.likes > 0 ?
                            <span>
                              <strong>{img.likes}</strong>
                              <ThumbUpAltOutlinedIcon/>
                            </span>
                            :
                            ''
                          }
                          actionIcon={
                            activeUser && activeUser.username === post.name ?
                            <DeleteForeverOutlinedIcon
                              className={classes.icon}
                              onClick={() => { handleDeleteImage(post.cuid, img.publicId) }}
                            />
                            :
                            <ThumbUpAltOutlinedIcon
                              onClick={() => { handleLikeImage(post.cuid, img.publicId) }}
                            />
                          }
                        />
                      </GridListTile>
                    ))}
                  </GridList>
                </div>
                :
                'The gallery is empty'
            }
          </div>
        </div>
        {
          activeUser && activeUser.username === post.name?
          <div className="row">
            <div className="col-12">
              <PostAddImageWidget
                addImage={handleAddImage}
                activeUser={activeUser}
                />
            </div>
          </div>
          :
          ''
        }
      </div>
      )
    : (<div>Loading</div>)
  );
}

export default PostDetailPage;