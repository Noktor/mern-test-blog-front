import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

// Import Components
import PostList from '../../components/PostList';
import PostCreateWidget from '../../components/PostCreateWidget';

// Import Actions
import { addPostRequest, deletePostRequest, likePostRequest, fetchPosts } from '../../PostActions';
import { loadUserRequest } from '../../../Session/SessionActions';

// Import Resources
import Logo from '../../../logo.svg';

// Import constants
import { USER_LOADING, AUTH_ERROR } from '../../../util/actionTypes';

const PostListPage = ({ showAddPost }) => {

  const dispatch = useDispatch();
  const posts = useSelector(state => state.posts.data);
  const token = useSelector(state => state.auth.token);
  const username = useSelector(state => state.auth.user);

  useEffect(() => {
    dispatch(fetchPosts());
    dispatch({ type: USER_LOADING });
    dispatch(loadUserRequest(token));
  },[]);

  const handleDeletePost = (post) => {
    if (confirm('Do you want to delete this post')) { // eslint-disable-line
      dispatch(deletePostRequest(post, token));
      dispatch(fetchPosts());
    }
  };

  const handleLikePost = (post) => {
    if (confirm('Do you want to like this post')) { // eslint-disable-line
      dispatch(likePostRequest(post, token));
      dispatch(fetchPosts());
    }
  }

  const handleAddPost = (post, token) => {
    dispatch(addPostRequest(post, token));
    dispatch(fetchPosts());
  };

  return (
    <div className="container">
      <div className="row">
        <div className="col-12 d-flex align-items-center">
          <img className="mx-3" src={Logo} alt="Logo" style={{ height: '72px'}}/>
          <h1 className="mt-4">
             Alaya Blog
          </h1>
        </div>
      </div>
      <hr />
      <div className="row">
        <div className="col-6">
          {
            token?
            <PostCreateWidget addPost={handleAddPost} showAddPost={showAddPost} token={token} />
            : ''
          }  
        </div>
        <div className="col-6">
          <PostList handleDeletePost={handleDeletePost} handleLikePost={handleLikePost} posts={posts} token={token} activeUser={username} />
        </div>
      </div>
    </div>
  );
};

export default PostListPage;