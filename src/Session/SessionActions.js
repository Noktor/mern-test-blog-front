import callApi from '../util/apiCaller';
import { returnErrors } from '../util/ErrorActions';

// Import Constants
import {
  USER_LOADED,
  USER_LOADING,
  AUTH_ERROR,
  LOGIN_SUCCESS,
  LOGIN_FAIL,
  LOGOUT_SUCCESS,
  REGISTER_SUCCESS,
  REGISTER_FAIL,
  SET_NEW_AVATAR
} from "../util/actionTypes";

// Export Actions
export function loadUser(res) {
  return {
    type: USER_LOADED,
    payload: res,
  };
}

export function loadUserRequest(token) {
  return (dispatch) => {
    return callApi('users/user', 'get', null, token)
    .then(res => dispatch(loadUser(res)));
  };
}

export function registerUser(res) {
  return {
    type: REGISTER_SUCCESS,
    payload: res
  };
}

export function registerUserRequest(user) {
  return (dispatch) => {
    return callApi('users/register', 'post', user).then(res => dispatch(registerUser(res)));
  };
}

export function loginUser(res) {
  return {
    type: LOGIN_SUCCESS,
    payload: res
  };
}

export function loginUserRequest(user) {
  return (dispatch) => {
    return callApi('users/login', 'post', user).then(res => dispatch(loginUser(res)));
  };
}

export function setNewAvatar(res) {
  return {
    type: SET_NEW_AVATAR,
    payload: res,
  };
}

export function setNewAvatarRequest(data, token) {
  return (dispatch) => {
    return callApi('users/avatar', 'post', {
      data
    }, token).then(res => dispatch(setNewAvatar(res)));
  };
}

export function logoutUser() {
  return {
    type: LOGOUT_SUCCESS
  };
}