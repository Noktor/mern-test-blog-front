import React, { Fragment } from 'react';
import Link from '@material-ui/core/Link';
import PropTypes from 'prop-types';

export const SessionLogoutFragment = ({ logoutUser }) => {

    const handleLogout = () => {
        logoutUser();
    };

    return (
        <Fragment>
            <Link onClick={handleLogout} href="#" className="text-white">Logout</Link>
        </Fragment>
    );
};

SessionLogoutFragment.propTypes = {
    // error: PropTypes.object.isRequired,
    logoutUser: PropTypes.func.isRequired,
};

export default SessionLogoutFragment;