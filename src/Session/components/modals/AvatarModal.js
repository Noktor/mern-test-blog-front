import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { 
  Button,
  TextField,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Avatar
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { useSelector } from 'react-redux';

// Import Style
const useStyles = makeStyles(theme => ({

}));

const AvatarModal = ({ setNewAvatar }) => {

  const [state, setState] = useState({});
  const [selectedFile, setSelectedFile] = useState();

  const [open, setOpen] = React.useState(false);
  const authState = useSelector(state => state);
  const classes = useStyles();

  const handleClickOpen = () => {
    setOpen(true);
  }

  const handleClickClose = () => {
    setOpen(false);
  }

  const fileSelectedHandler = (e) => {
    const file = e.target.files[0];
    setSelectedFile(file);
  }

  const handleSetNewAvatar = (e) => {
    e.preventDefault();
    if (!selectedFile) return;
    const reader = new FileReader();
    reader.readAsDataURL(selectedFile);
    reader.onloadend = () => {
        uploadImage(reader.result);
        setOpen(false);
    };
    reader.onerror = () => {
    };
  };

  const uploadImage = async (base64EncodedImage) => {
    setState({
      ...state,
      image: base64EncodedImage
    })
    setNewAvatar({image: base64EncodedImage});
  };

  return (
    <div>
        {
          authState !== null && authState.auth.user.avatar ?
          <Avatar src={authState.auth.user.avatar.url} style={{cursor: 'pointer'}} onClick={handleClickOpen}/>
          :
          <Avatar style={{cursor: 'pointer'}} onClick={handleClickOpen}/>
        }
        <Dialog open={open} onClose={handleClickClose} aria-labelledby="form-dialog-title">
          <form encType="multipart/form-data">

            <DialogTitle id="form-dialog-title">Set new avatar</DialogTitle>
            <DialogContent>
            <div>
                <input type="file" onChange={fileSelectedHandler}/>
            </div>
            </DialogContent>
            <DialogActions>
              <Button onClick={handleClickClose} color="primary">
                Cancel
              </Button>
              <Button onClick={handleSetNewAvatar} color="primary">
                Set new avatar
              </Button>
            </DialogActions>
            </form>
        </Dialog>
    </div>
  );
};

AvatarModal.propTypes = {
  // error: PropTypes.object.isRequired,
  setNewAvatar: PropTypes.func.isRequired
};

export default AvatarModal;
