import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { 
  Button,
  TextField,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { useSelector } from 'react-redux';

// Import Style
const useStyles = makeStyles(theme => ({
    root: {
        '& > *': {
            margin: theme.spacing(1),
        },
    },
}));

const SessionLoginModal = ({ loginUser }) => {
  const [state, setState] = useState();
  const [open, setOpen] = React.useState(false);
  const authState = useSelector(state => state);

  const handleClickOpen = () => {
    setOpen(true);
  }

  const handleClickClose = () => {
    setOpen(false);
  }

  const handleLogin = () => {
    if (state.email && state.password) {
      loginUser(state);
    }
  };

  const handleChange = (evt) => {
    const value = evt.target.value;
    setState({
        ...state,
        [evt.target.name]: value
    });
  };

  return (
    <div>
        {
          authState !== null ?
          <Button variant="outlined" color="inherit" onClick={handleClickOpen}>
            Login
          </Button>
          :
          ''
        }
        <Dialog open={open} onClose={handleClickClose} aria-labelledby="form-dialog-title">
          <DialogTitle id="form-dialog-title">Login</DialogTitle>
          <DialogContent>
            <DialogContentText>
              Welcome to the Alaya blog app!
            </DialogContentText>
            <TextField
              autoFocus
              margin="dense"
              name="email"
              label="Email Address"
              type="email"
              fullWidth
              onChange={handleChange}
            />
            <TextField
              margin="dense"
              label="Password"
              name="password"
              type="password"
              fullWidth
              onChange={handleChange}
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={handleClickClose} color="primary">
              Cancel
            </Button>
            <Button onClick={handleLogin} color="primary">
              Login
            </Button>
          </DialogActions>
        </Dialog>
    </div>
  );
};

SessionLoginModal.propTypes = {
  // error: PropTypes.object.isRequired,
  loginUser: PropTypes.func.isRequired
};

export default SessionLoginModal;
