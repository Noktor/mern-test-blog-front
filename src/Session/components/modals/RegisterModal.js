import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { 
  Button,
  TextField,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle
} from '@material-ui/core';
import { useSelector } from 'react-redux';

//falta posar styles si s'escau:

const SessionRegisterModal = ({ registerUser }) => {

  const [state, setState] = useState({});
  const [open, setOpen] = React.useState(false);
  const authState = useSelector(state => state);

  const handleClickOpen = (evt) => {
    setOpen(true);
  }

  const handleClickClose = (evt) => {
    setOpen(false);
  }

  const handleRegister = () => {
    if (state.email && state.username && state.password) {
      registerUser(state);
    }
  };

  const handleChange = (evt) => {
    const value = evt.target.value;
    setState({
        ...state,
        [evt.target.name]: value
    });
  };

  return (
    <div>
      {
        authState !== null ?
          <Button variant="outlined" color="inherit" onClick={handleClickOpen}>
            Register
          </Button>
        :
        ''
      }
      <Dialog open={open} onClose={handleClickClose} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Register</DialogTitle>
        <DialogContent>
          <DialogContentText>
            Welcome to the Alaya blog app!
          </DialogContentText>
          <TextField
            autoFocus
            margin="dense"
            name="email"
            label="Email Address"
            type="email"
            fullWidth
            onChange={handleChange}
          />
          <TextField
            margin="dense"
            label="Username"
            name="username"
            type="text"
            fullWidth
            onChange={handleChange}
          />
          <TextField
            margin="dense"
            label="Password"
            name="password"
            type="password"
            fullWidth
            onChange={handleChange}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClickClose} color="primary">
            Cancel
          </Button>
          <Button onClick={handleRegister} color="primary">
            Register
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

SessionRegisterModal.propTypes = {
  // error: PropTypes.object.isRequired,
  registerUser: PropTypes.func.isRequired,
};

export default SessionRegisterModal;
