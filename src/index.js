import React from 'react';
import * as ReactDOM from 'react-dom';
import { combineReducers, createStore, compose, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import posts from './Post/PostReducer';
import auth from './Session/AuthReducer';
import error from './util/ErrorReducer';
import './index.css';
import App from './App';

// Middleware and store enhancers
const enhancers = [
    applyMiddleware(thunk),
    window.devToolsExtension ? window.devToolsExtension() : f => f
];
const initialStore = createStore(combineReducers({ posts, auth, error }), { }, compose(...enhancers));
ReactDOM.render(<App store={initialStore}/>, document.getElementById('root'));