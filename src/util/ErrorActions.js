import callApi from './apiCaller';

// Import Constants
import {
  GET_ERRORS,
  CLEAR_ERRORS,
} from "./actionTypes";

// Export Actions
export function returnErrors(msg, status, id = null) {
  return {
    type: GET_ERRORS,
    payload: { msg, status, id }
  };
}

export function clearErrors() {
  return {
    type: CLEAR_ERRORS
  }
}