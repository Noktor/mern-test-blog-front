import fetch from 'isomorphic-fetch';

export const API_URL = `${process.env.API_URL}/api` || 'http://localhost:3000/api';
export default async (endpoint, method = 'get', body = null, token = null) => {
  let options = {}
  options.headers = { 'Content-Type': 'application/json' };
  options.method = method;
  
  if(body) options.body = JSON.stringify(body);
  if(token) options.headers['x-auth-token'] = token;

  return fetch(`${API_URL}/${endpoint}`, options)
  .then(response => response.json().then(json => ({ json, response })))
  .then(({ json, response }) => {
    if (!response.ok) {
      return Promise.reject(json);
    }
    return json;
  })
  .then(
    response => response,
    error => error
  );
}